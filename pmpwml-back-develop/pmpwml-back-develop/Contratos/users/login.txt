//========ENDPOINT
//http://localhost:5000/api/v1/users/login
//========METODO
POST
//========REQUEST-BODY
{
    "username":"admin001",
    "password":"Lima123$"
}
//========RESPONSE-BODY - SI ESTA CORRECTO EL LOGUEO
{
    "token": "jdkasjdlakjdlasd###",
    "userType": 1,
    "username": "admin001"
}
//========RESPONSE-BODY - SI FALTA INGRESAR USERNAME O PASSWORD - 400
{
    "msg": "BadRequest: Missing JSON data in request (username, password)"
}
//========RESPONSE-BODY - SI FALLAN LAS CREDENCIALES - 401
{
    "status": "Unathorized"
}