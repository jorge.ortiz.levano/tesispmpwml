//========ENDPOINT
//http://localhost:5000/api/v1/sales/list
//========METODO
POST
//========HEADER:
token:"jdkasjdlakjdlasd###"
//========REQUEST-BODY
//========RESPONSE-BODY - CORRECTO - 200
{
    "response": [
        {
            "CANTIDAD": 20,
            "FECHA": "2021-06-11 05:09:21.000000",
            "ID_CLIENTE": 2,
            "ID_PRODUCTO": 1,
            "ID_VENTA": 5,
            "MONTO": 200.0
        },
        {
            "CANTIDAD": 30,
            "FECHA": "2021-06-11 05:10:22.000000",
            "ID_CLIENTE": 2,
            "ID_PRODUCTO": 1,
            "ID_VENTA": 6,
            "MONTO": 300.0
        }
    ]
}
//========RESPONSE-BODY - SI FALTA O MAL TOKEN - 401
{
    "status": "Unathorized"
}
//========RESPONSE-BODY - ERROR INTERNO (BACK O BD) - 500
{
    "msg": "Internal Error"
}