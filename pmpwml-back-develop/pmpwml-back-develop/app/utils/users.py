from flask_mysqldb import MySQL
import smtplib

#<================================func-users======================================>
def f_users_login(mysql,username,password):
    try:
        #query bd
        cur = mysql.connection.cursor()
        cur.execute("SELECT ID_ROL FROM T_USUARIO WHERE USERNAME=%s AND PASSWORD=%s",(username,password))
        rv = cur.fetchall()
        cur.close()
        userType=rv[0][0]
    except:
        userType=0
    return userType

def f_users_password(username):
    try:
        #query bd
        ############SEND MAIL
        sender_email = 'alcala.testcode@gmail.com'
        rec_email = 'jorge.ortiz.levano@gmail.com'
        #rec_email = 'alcala.util@gmail.com'
        password = 'testcode123'
        subject= 'Solicitud de restablecimiento de contraseNa'
        #definimos el body del mensaje
        mensaje= 'Estimado administrador,\n\nEl usuario '+username+' solicita se le restablezca la contraseNa.\n\nGracias,\nSaludos'
        #fin de definicion del body del mensaje
        message = 'Subject: {}\n\n{}'.format(subject, mensaje)
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(sender_email, password)
        server.sendmail(sender_email,rec_email,message)
        server.quit()
        success=1
    except:
        success=0
    return success

def f_users_list(mysql):
    try:
        #query bd
        cur = mysql.connection.cursor()
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('ID_USUARIO', ID_USUARIO, 'ID_ROL', ID_ROL, 'USERNAME', USERNAME,'NOMBRE', NOMBRE, 'APELLIDO', APELLIDO, 'FECHA_CREACION', FECHA_CREACION)) from T_USUARIO;")
        rv = cur.fetchone()
        cur.close()
        success=rv[0]
        if success == None:
            success='[]'
    except:
        success='0'
    return success

def f_users_add(mysql,username,userType,password,nombre,apellido):
    try:
        sql = "INSERT INTO T_USUARIO (USERNAME, ID_ROL, PASSWORD, NOMBRE, APELLIDO) VALUES (%s,%s,%s,%s,%s)"
        val = (username,int(userType),str(password),nombre,apellido)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success

def f_users_update(mysql,username,userType,password,nombre,apellido,userid):
    try:
        sql = "UPDATE T_USUARIO SET USERNAME=%s, ID_ROL=%s, PASSWORD=%s, NOMBRE=%s, APELLIDO=%s WHERE ID_USUARIO = %s"
        val = (username,int(userType),str(password),nombre,apellido,userid)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success

def f_users_delete(mysql,userid):
    try:
        sql = "DELETE FROM T_USUARIO WHERE ID_USUARIO = %s"
        val = (int(userid),)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success
