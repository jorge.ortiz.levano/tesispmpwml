from flask_mysqldb import MySQL
import smtplib

#<================================func-purchases======================================>
def f_purchases_list(mysql):
    try:
        #query bd
        cur = mysql.connection.cursor()
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('ID_ORDEN_COMPRA', ID_ORDEN_COMPRA, 'ID_RESULTADO_MODELO', ID_RESULTADO_MODELO, 'ID_PRODUCTO', ID_PRODUCTO,'PROVEEDOR', PROVEEDOR, 'MENSAJE', MENSAJE, 'FECHA', FECHA)) from T_ORDEN_COMPRA;")
        rv = cur.fetchone()
        cur.close()
        success=rv[0]
        if success == None:
            success='[]'
    except:
        success='0'
    return success

def f_purchases_add(mysql,modeloid,productid,proovedor,mensaje):
    try:
        cur = mysql.connection.cursor()
        #obtenemos la cantidad aNadida a partir del modelo-id
        cur.execute("SELECT PREDICCION FROM T_RESULTADO_MODELO WHERE ID_RESULTADO_MODELO=%s",(modeloid,))
        rv = cur.fetchall()
        prediccion=int(rv[0][0])
        #añadimos el registro a la tabla compras
        sql = "INSERT INTO T_ORDEN_COMPRA (ID_RESULTADO_MODELO, ID_PRODUCTO, PROVEEDOR, MENSAJE) VALUES (%s,%s,%s,%s)"
        val = (modeloid,productid,proovedor,mensaje)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        #ACTUALIZAMOS EL INVENTARIO
        cur.execute("SELECT STOCK FROM T_PRODUCTO WHERE ID_PRODUCTO=%s",(productid,))
        rv = cur.fetchall()
        stock=int(rv[0][0])
        stock=stock+prediccion
        cur.execute("UPDATE T_PRODUCTO SET STOCK=%s WHERE ID_PRODUCTO=%s",(stock,productid))
        #obtenemos el nombre del producto para el correo
        cur.execute("SELECT NOMBRE_PRODUCTO FROM T_PRODUCTO WHERE ID_PRODUCTO=%s",(productid,))
        rv = cur.fetchall()
        nombreProducto=rv[0][0]
        #cerramos conexion a la bd
        mysql.connection.commit()
        cur.close()
        ############SEND MAIL
        sender_email = 'alcala.testcode@gmail.com'
        rec_email = 'jorge.ortiz.levano@gmail.com'
        #rec_email = 'alcala.util@gmail.com'
        password = 'testcode123'
        subject= 'Solicitud de compra prueba COVID19'
        #definimos el body del mensaje
        mensaje= mensaje+'\n\nProducto: '+nombreProducto+'\nCantidad: '+str(prediccion)+'\n\nGracias,\nSaludos'
        #fin de definicion del body del mensaje
        message = 'Subject: {}\n\n{}'.format(subject, mensaje)
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(sender_email, password)
        server.sendmail(sender_email,rec_email,message)
        server.quit()
        success=1
    except:
        success=0
    return success

def f_purchases_delete(mysql,purchaseid):
    try:
        sql = "DELETE FROM T_ORDEN_COMPRA WHERE ID_ORDEN_COMPRA = %s"
        val = (int(purchaseid),)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success
