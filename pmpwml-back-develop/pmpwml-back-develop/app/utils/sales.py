from flask_mysqldb import MySQL

#<================================func-sales======================================>
def f_sales_list(mysql):
    try:
        #query bd
        cur = mysql.connection.cursor()
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('ID_VENTA', ID_VENTA, 'ID_CLIENTE', ID_CLIENTE, 'ID_PRODUCTO', ID_PRODUCTO,'CANTIDAD', CANTIDAD, 'MONTO', MONTO, 'FECHA', FECHA)) from T_VENTA;")
        rv = cur.fetchone()
        cur.close()
        success=rv[0]
        if success == None:
            success='[]'
    except:
        success='0'
    return success

def f_sales_add(mysql,clientid,productid,cantidad):
    #try:
    cur = mysql.connection.cursor()
    #TRAEMOS EL PRECIO DEL PRODUCTO PARA CALULAR MONTO
    cur.execute("SELECT PRECIO FROM T_PRODUCTO WHERE ID_PRODUCTO=%s",(productid,))
    rv = cur.fetchall()
    productPrice=rv[0][0]
    monto=float(productPrice)*int(cantidad)
    #REGISTRAMOS LA VENTA
    sql = "INSERT INTO T_VENTA (ID_CLIENTE, ID_PRODUCTO, CANTIDAD, MONTO) VALUES (%s,%s,%s,%s)"
    val = (clientid,productid,cantidad,monto)
    cur.execute(sql, val)
    #ACTUALIZAMOS EL INVENTARIO
    cur.execute("SELECT STOCK FROM T_PRODUCTO WHERE ID_PRODUCTO=%s",(productid,))
    rv = cur.fetchall()
    stock=int(rv[0][0])
    stock=stock-cantidad
    cur.execute("UPDATE T_PRODUCTO SET STOCK=%s WHERE ID_PRODUCTO=%s",(stock,productid))
    #cerramos conexion a la bd
    mysql.connection.commit()
    cur.close()
    success=1
    #except:
    #    success=0
    return success

def f_sales_delete(mysql,salesid):
    try:
        sql = "DELETE FROM T_VENTA WHERE ID_VENTA = %s"
        val = (int(salesid),)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success
