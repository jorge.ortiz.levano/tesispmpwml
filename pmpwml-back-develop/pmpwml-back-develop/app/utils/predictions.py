from flask_mysqldb import MySQL

#<================================func-predictions======================================>
def f_predictions_list(mysql):
    try:
        #query bd
        cur = mysql.connection.cursor()
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('ID_RESULTADO_MODELO', ID_RESULTADO_MODELO, 'NOMBRE', NOMBRE, 'MES', MES,'ANO', ANO,'PREDICCION', PREDICCION,'PRECISION_MODELO', PRECISION_MODELO)) from T_RESULTADO_MODELO;")
        rv = cur.fetchone()
        cur.close()
        success=rv[0]
        if success == None:
            success='[]'
    except:
        success='0'
    return success

def f_predictions_add(mysql,nombre,mes,ano,prediccion,precision):
    try:
        sql = "INSERT INTO T_RESULTADO_MODELO (NOMBRE, MES, ANO, PREDICCION, PRECISION_MODELO) VALUES (%s,%s,%s,%s,%s)"
        val = (nombre,mes,ano,prediccion,precision)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success

def f_predictions_update(mysql,nombre,mes,ano,prediccion,precision,predictionid):
    try:
        sql = "UPDATE T_RESULTADO_MODELO SET NOMBRE=%s, MES=%s, ANO=%s, PREDICCION=%s, PRECISION_MODELO=%s WHERE ID_RESULTADO_MODELO = %s"
        val = (nombre,mes,ano,prediccion,precision,predictionid)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success

def f_predictions_delete(mysql,predictionid):
    try:
        sql = "DELETE FROM T_RESULTADO_MODELO WHERE ID_RESULTADO_MODELO = %s"
        val = (int(predictionid),)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success