from flask_mysqldb import MySQL
from flask import json
#<================================func-reports======================================>

def f_reports_list(mysql):
    try:
        #iniciamos la conexion a la bd
        cur = mysql.connection.cursor()
        #extraemos ventas
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('FECHA', FECHA, 'CANTIDAD', CANTIDAD, 'MONTO', MONTO)) from T_VENTA;")
        rv = cur.fetchone()
        l_ventas_0=json.loads(rv[0])
        #extraemos compras
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('FECHA', FECHA, 'ID_RESULTADO_MODELO', ID_RESULTADO_MODELO)) from T_ORDEN_COMPRA;")
        rv = cur.fetchone()
        l_compras_0=json.loads(rv[0])
        #extraemos predicciones
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('PREDICCION', PREDICCION, 'ID_RESULTADO_MODELO', ID_RESULTADO_MODELO)) from T_RESULTADO_MODELO;")
        rv = cur.fetchone()
        l_predicciones_0=json.loads(rv[0])
        #cerramos la conexion a la bd
        cur.close()
        #convertimos las predicciones a una lista simple para consultas futuras
        l_predicciones=[None] * 50
        for item in l_predicciones_0:
            l_predicciones[item['ID_RESULTADO_MODELO']]=item['PREDICCION']
        #conseguimos las compras por mes
        l_compras=[0] * 13
        for item in l_compras_0:
            if item['FECHA'][0:4] == '2021':
                mes=int(item['FECHA'][5:7])
                l_compras[mes]=l_compras[mes]+int(l_predicciones[item['ID_RESULTADO_MODELO']])
        #conseguimos las ventas por mes
        l_ventas=[0] * 13
        l_ventas_monto=[0] * 13
        for item in l_ventas_0:
            if item['FECHA'][0:4] == '2021':
                mes=int(item['FECHA'][5:7])
                l_ventas_monto[mes]=l_ventas_monto[mes]+int(item['MONTO'])
                l_ventas[mes]=l_ventas[mes]+int(item['CANTIDAD'])
        #generamos la lista a enviar al reporte
        success=[None] * 12
        for i in range(1,13):
            item={}
            item['mes']=i
            item['compras']=l_compras[i]
            item['ventas']=l_ventas[i]
            item['dinero_ventas']=l_ventas_monto[i]
            success[i-1]=item
    except:
        success='0'
    return success

def f_compras_anual(success):
    ca=0
    for i in range(12):
        ca=ca+success[i]['compras']
    return ca

def f_ventas_anual(success):
    va=0
    for i in range(12):
        va=va+success[i]['ventas']
    return va