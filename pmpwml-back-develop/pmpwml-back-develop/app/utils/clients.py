from flask_mysqldb import MySQL

#<================================func-clients======================================>
def f_clients_list(mysql):
    try:
        #query bd
        cur = mysql.connection.cursor()
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('ID_CLIENTE', ID_CLIENTE, 'NOMBRE', NOMBRE, 'APELLIDO', APELLIDO,'TIPO_DOCUMENTO', TIPO_DOCUMENTO, 'NUM_DOCUMENTO', NUM_DOCUMENTO, 'SEXO', SEXO, 'DIRECCION', DIRECCION, 'DISTRITO', DISTRITO, 'TELEFONO', TELEFONO, 'EDAD', EDAD, 'EMAIL', EMAIL)) from T_CLIENTE;")
        rv = cur.fetchone()
        cur.close()
        success=rv[0]
        if success == None:
            success='[]'
    except:
        success='0'
    return success

def f_clients_add(mysql,nombre,apellido,tipoDocumento,numDocumento,sexo,direcion,distrito,telefono,edad,email):
    try:
        sql = "INSERT INTO T_CLIENTE (ID_CLIENTE, NOMBRE, APELLIDO, TIPO_DOCUMENTO, NUM_DOCUMENTO, SEXO, DIRECCION, DISTRITO, TELEFONO, EDAD, EMAIL) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        val = (int(numDocumento),nombre,apellido,str(tipoDocumento),str(numDocumento),sexo,direcion,distrito,str(telefono),edad,email)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success

def f_clients_update(mysql,nombre,apellido,tipoDocumento,numDocumento,sexo,direcion,distrito,telefono,edad,email,clientid):
    try:
        sql = "UPDATE T_CLIENTE SET NOMBRE=%s, APELLIDO=%s, TIPO_DOCUMENTO=%s, NUM_DOCUMENTO=%s, SEXO=%s, DIRECCION=%s, DISTRITO=%s, TELEFONO=%s, EDAD=%s, EMAIL=%s WHERE ID_CLIENTE=%s"
        val = (nombre,apellido,str(tipoDocumento),str(numDocumento),sexo,direcion,distrito,str(telefono),edad,email,clientid)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success

def f_clients_delete(mysql,clientid):
    try:
        sql = "DELETE FROM T_CLIENTE WHERE ID_CLIENTE = %s"
        val = (int(clientid),)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success