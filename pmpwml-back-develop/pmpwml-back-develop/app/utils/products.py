from flask_mysqldb import MySQL

#<================================func-products======================================>
def f_products_list(mysql):
    try:
        #query bd
        cur = mysql.connection.cursor()
        cur.execute("SELECT JSON_ARRAYAGG(JSON_OBJECT('ID_PRODUCTO', ID_PRODUCTO, 'NOMBRE_PRODUCTO', NOMBRE_PRODUCTO, 'PRECIO', PRECIO,'STOCK', STOCK)) from T_PRODUCTO;")
        rv = cur.fetchone()
        cur.close()
        success=rv[0]
        if success == None:
            success='[]'
    except:
        success='0'
    return success

def f_products_add(mysql,nombre,precio,stock):
    try:
        sql = "INSERT INTO T_PRODUCTO (NOMBRE_PRODUCTO, PRECIO, STOCK) VALUES (%s,%s,%s)"
        val = (nombre,precio,stock)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success

def f_products_delete(mysql,productid):
    try:
        sql = "DELETE FROM T_PRODUCTO WHERE ID_PRODUCTO = %s"
        val = (int(productid),)
        cur = mysql.connection.cursor()
        cur.execute(sql, val)
        mysql.connection.commit()
        cur.close()
        success=1
    except:
        success=0
    return success