from flask import Flask, jsonify, request, json
from flask_mysqldb import MySQL
from flask_cors import CORS
from utils.users import *
from utils.clients import *
from utils.products import *
from utils.sales import *
from utils.purchases import *
from utils.predictions import *
from utils.reports import *

app = Flask(__name__)
CORS(app)
#<================================DB-CONFIG======================================>
#https://www.codementor.io/@adityamalviya/python-flask-mysql-connection-rxblpje73
#https://www.w3schools.com/php/php_mysql_connect.asp
#https://app.diagrams.net/#G1cxat81xejx8O3X4f2nNsAOE5L_2gVacA
app.config['MYSQL_HOST'] = 'pmpwml.mysql.database.azure.com'
app.config['MYSQL_USER'] = 'admin123'
app.config['MYSQL_PASSWORD'] = 'Lima123$'
app.config['MYSQL_DB'] = 'pmpwml'
mysql = MySQL(app)
app.config['TOKEN_VALUE'] = 'jdkasjdlakjdlasd###'
#<================================healt-check======================================>
@app.route('/api/v1/healthz', methods=['POST'])
def healthz():
  return jsonify(status="OK"), 200

#<================================INICIO-func-users======================================>
@app.route('/api/v1/users/login', methods=['POST'])
def users_login():
  #revisamos el correcto envIo de parAmetros
  try:
    username = request.json.get('username', None)
    password = request.json.get('password', None)
  except:
    return jsonify(msg="BadRequest: Missing JSON data in request (username, password)"), 400
  #realizamos el login
  userType=f_users_login(mysql,username,password)
  if userType == 0:
    return jsonify(status="Unathorized"), 401
  else:
    return jsonify(username=username,userType=userType,token=app.config['TOKEN_VALUE']), 200

@app.route('/api/v1/users/password', methods=['POST'])
def users_password():
  #revisamos el correcto envIo de parAmetros
  try:
    username = request.json.get('username', None)
  except:
    return jsonify(msg="BadRequest: Missing JSON data in request (username)"), 400
  #realizamos el login
  success=f_users_password(username)
  if success == 0:
    return jsonify(response='Internal error'), 500
  else:
    return jsonify(status="OK"), 200

@app.route('/api/v1/users/list', methods=['POST'])
def users_list():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API
      #realizamos la lectura de usuarios
      success=f_users_list(mysql)
      if success != '0':
        return jsonify(response=json.loads(success)), 200
      else:
        return jsonify(response='Internal error'), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/users/add', methods=['POST'])
def users_add():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        username = request.json.get('username', None)
        userType = request.json.get('userType', None)
        password = request.json.get('password', None)
        nombre = request.json.get('nombre', None)
        apellido = request.json.get('apellido', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_users_add(mysql,username,userType,password,nombre,apellido)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/users/update', methods=['POST'])
def users_update():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        userid = request.json.get('userid', None)
        username = request.json.get('username', None)
        userType = request.json.get('userType', None)
        password = request.json.get('password', None)
        nombre = request.json.get('nombre', None)
        apellido = request.json.get('apellido', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_users_update(mysql,username,userType,password,nombre,apellido,userid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/users/delete', methods=['POST'])
def users_delete():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        userid = request.json.get('userid', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_users_delete(mysql,userid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN
#<================================FIN-func-users======================================>

#<================================INICIO-func-clients======================================>
@app.route('/api/v1/clients/list', methods=['POST'])
def clients_list():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API
      #realizamos la lectura de clientes
      success=f_clients_list(mysql)
      if success != '0':
        return jsonify(response=json.loads(success)), 200
      else:
        return jsonify(response='Internal error'), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/clients/add', methods=['POST'])
def clients_add():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        nombre = request.json.get('nombre', None)
        apellido = request.json.get('apellido', None)
        tipoDocumento = request.json.get('tipoDocumento', None)
        numDocumento = request.json.get('numDocumento', None)
        sexo = request.json.get('sexo', None)
        direcion = request.json.get('direcion', None)
        distrito = request.json.get('distrito', None)
        telefono = request.json.get('telefono', None)
        edad = request.json.get('edad', None)
        email = request.json.get('email', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_clients_add(mysql,nombre,apellido,tipoDocumento,numDocumento,sexo,direcion,distrito,telefono,edad,email)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/clients/update', methods=['POST'])
def clients_update():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        clientid = request.json.get('clientid', None)
        nombre = request.json.get('nombre', None)
        apellido = request.json.get('apellido', None)
        tipoDocumento = request.json.get('tipoDocumento', None)
        numDocumento = request.json.get('numDocumento', None)
        sexo = request.json.get('sexo', None)
        direcion = request.json.get('direcion', None)
        distrito = request.json.get('distrito', None)
        telefono = request.json.get('telefono', None)
        edad = request.json.get('edad', None)
        email = request.json.get('email', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_clients_update(mysql,nombre,apellido,tipoDocumento,numDocumento,sexo,direcion,distrito,telefono,edad,email,clientid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/clients/delete', methods=['POST'])
def clients_delete():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        clientid = request.json.get('clientid', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_clients_delete(mysql,clientid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN
#<================================FIN-func-clients======================================>

#<================================INICIO-func-products======================================>
@app.route('/api/v1/products/list', methods=['POST'])
def products_list():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API
      #realizamos la lectura de products
      success=f_products_list(mysql)
      if success != '0':
        return jsonify(response=json.loads(success)), 200
      else:
        return jsonify(response='Internal error'), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/products/add', methods=['POST'])
def products_add():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        nombre = request.json.get('nombre', None)
        precio = request.json.get('precio', None)
        stock = request.json.get('stock', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_products_add(mysql,nombre,precio,stock)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/products/delete', methods=['POST'])
def products_delete():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        productid = request.json.get('productid', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_products_delete(mysql,productid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN
#<================================FIN-func-products======================================>

#<================================INICIO-func-sales======================================>
@app.route('/api/v1/sales/list', methods=['POST'])
def sales_list():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API
      #realizamos la lectura de sales
      success=f_sales_list(mysql)
      if success != '0':
        return jsonify(response=json.loads(success)), 200
      else:
        return jsonify(response='Internal error'), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/sales/add', methods=['POST'])
def sales_add():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        clientid = request.json.get('clientid', None)
        productid = request.json.get('productid', None)
        cantidad = request.json.get('cantidad', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_sales_add(mysql,clientid,productid,cantidad)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/sales/delete', methods=['POST'])
def sales_delete():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        salesid = request.json.get('salesid', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_sales_delete(mysql,salesid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN
#<================================FIN-func-sales======================================>

#<================================INICIO-func-purchases======================================>
@app.route('/api/v1/purchases/list', methods=['POST'])
def purchases_list():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API
      #realizamos la lectura de sales
      success=f_purchases_list(mysql)
      if success != '0':
        return jsonify(response=json.loads(success)), 200
      else:
        return jsonify(response='Internal error'), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/purchases/add', methods=['POST'])
def purchases_add():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        modeloid = request.json.get('modeloid', None)
        productid = request.json.get('productid', None)
        proovedor = request.json.get('proovedor', None)
        mensaje = request.json.get('mensaje', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_purchases_add(mysql,modeloid,productid,proovedor,mensaje)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN
  
@app.route('/api/v1/purchases/delete', methods=['POST'])
def purchases_delete():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        purchaseid = request.json.get('purchaseid', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_purchases_delete(mysql,purchaseid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN
#<================================FIN-func-purchases======================================>

#<================================INICIO-func-predictions======================================>
@app.route('/api/v1/predictions/list', methods=['POST'])
def predictions_list():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API
      #realizamos la lectura de sales
      success=f_predictions_list(mysql)
      if success != '0':
        return jsonify(response=json.loads(success)), 200
      else:
        return jsonify(response='Internal error'), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/predictions/add', methods=['POST'])
def predictions_add():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        nombre = request.json.get('nombre', None)
        mes = request.json.get('mes', None)
        ano = request.json.get('ano', None)
        prediccion = request.json.get('prediccion', None)
        precision = request.json.get('precision', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_predictions_add(mysql,nombre,mes,ano,prediccion,precision)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/predictions/update', methods=['POST'])
def predictions_update():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        predictionid = request.json.get('predictionid', None)
        nombre = request.json.get('nombre', None)
        mes = request.json.get('mes', None)
        ano = request.json.get('ano', None)
        prediccion = request.json.get('prediccion', None)
        precision = request.json.get('precision', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_predictions_update(mysql,nombre,mes,ano,prediccion,precision,predictionid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN

@app.route('/api/v1/predictions/delete', methods=['POST'])
def predictions_delete():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API      
      #revisamos el correcto envIo de parAmetros
      try:
        predictionid = request.json.get('predictionid', None)
      except:
        return jsonify(msg="BadRequest: Missing JSON data in request"), 400
      #realizamos el aNadido de usuario
      success=f_predictions_delete(mysql,predictionid)
      if success == 1:
        return jsonify(status="OK"), 200
      else:
        return jsonify(status="Internal Error"), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN
#<================================FIN-func-predictions======================================>

#<================================INICIO-func-reports======================================>
@app.route('/api/v1/reports/list', methods=['POST'])
def reports_list():
  ######VALIDACIoN-DE-TOKEN
  try:
    token = request.headers.get('token')
    if token == app.config['TOKEN_VALUE'] :
      ######INICIO-DE-API
      #realizamos la lectura de sales
      success=f_reports_list(mysql)
      va=f_ventas_anual(success)
      ca=f_compras_anual(success)
      if success != '0':
        return jsonify(ventas_anual=va,compras_anual=ca,mensual=success), 200
      else:
        return jsonify(response='Internal error'), 500
      ######FIN-DE-API
    else:
      return jsonify(status="Unathorized"), 401
  except:
    return jsonify(status="Unathorized"), 401
  ######FIN-VALIDACIoN-DE-TOKEN
#<================================FIN-func-reports======================================>

#<================================app-config======================================>
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)