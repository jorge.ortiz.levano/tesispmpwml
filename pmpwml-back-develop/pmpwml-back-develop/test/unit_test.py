from flask import Flask, jsonify, request, json
import requests , unittest

class TestCode(unittest.TestCase):
    URL = "http://localhost:5000/api/v1/"
    #<========================users========================>
    def test_user_list(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        resp = requests.post(self.URL+'users/list', headers=headers)
        self.assertEqual(resp.status_code,200)
    def test_user_add(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        json={"username": "admin001"}
        resp = requests.post(self.URL+'users/add', headers=headers, json=json)
        self.assertEqual(resp.status_code,500)
    def test_user_delete(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        json={"userid": 999}
        resp = requests.post(self.URL+'users/delete', headers=headers, json=json)
        self.assertEqual(resp.status_code,200)
    def test_user_password(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        json={"username": "admin001"}
        resp = requests.post(self.URL+'users/password', headers=headers, json=json)
        self.assertEqual(resp.status_code,200)
    def test_user_login(self):
        json={"username": "admin001", "password":"Lima123$"}
        resp = requests.post(self.URL+'users/login', json=json)
        self.assertEqual(resp.status_code,200)
    #<========================clients========================>
    def test_client_list(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        resp = requests.post(self.URL+'clients/list', headers=headers)
        self.assertEqual(resp.status_code,200)
    def test_client_add(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        json={"username": "admin001"}
        resp = requests.post(self.URL+'clients/add', headers=headers, json=json)
        self.assertEqual(resp.status_code,500)
    def test_client_delete(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        json={"clientid": 999}
        resp = requests.post(self.URL+'clients/delete', headers=headers, json=json)
        self.assertEqual(resp.status_code,200)
    #<========================products========================>
    def test_product_list(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        resp = requests.post(self.URL+'products/list', headers=headers)
        self.assertEqual(resp.status_code,200)
    def test_product_add(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        json={"username": "admin001"}
        resp = requests.post(self.URL+'products/add', headers=headers, json=json)
        self.assertEqual(resp.status_code,500)
    #<========================purchases========================>
    def test_purchase_list(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        resp = requests.post(self.URL+'purchases/list', headers=headers)
        self.assertEqual(resp.status_code,200)
    def test_purchase_add(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        json={"username": "admin001"}
        resp = requests.post(self.URL+'purchases/add', headers=headers, json=json)
        self.assertEqual(resp.status_code,500)
    #<========================sales========================>
    def test_sale_list(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        resp = requests.post(self.URL+'sales/list', headers=headers)
        self.assertEqual(resp.status_code,200)
    def test_sale_add(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        json={"username": "admin001"}
        resp = requests.post(self.URL+'sales/add', headers=headers, json=json)
        self.assertEqual(resp.status_code,401)
    #<========================predictions========================>
    def test_prediction_list(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        resp = requests.post(self.URL+'predictions/list', headers=headers)
        self.assertEqual(resp.status_code,200)
    #<========================reports========================>
    def test_report_list(self):
        headers={'token': 'jdkasjdlakjdlasd###'}
        resp = requests.post(self.URL+'reports/list', headers=headers)
        self.assertEqual(resp.status_code,200)

if __name__ == '__main__':
    unittest.main()